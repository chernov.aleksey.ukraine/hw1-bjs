// В JS  каждый обьект имеет внутреннюю ссылку на другой объект, который называется его прототипом.
// Прототип содеожит все возможные варианты свойств и методов, ссылаясь на которые и конкретизируя
//  их значения, можно создать любой объект - наследник.

// Ключевое слово super() используем для того, что бы иметь возможность воспользоваться свойствами
// объекта прототипа, (перечисляемыми в скобках, как аргументы) который мы используем для формирования
// класса наследника. Для пользования методами прототипа, используем .super, как метод (в случае
// необходимости переопределения таких методов). Но если методы новые, или использованный метод в
// прототипе перезаписывается полностью, .super не употребляем.
"use strict";
class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get name() {return this._name;}
  get age() {return this._age;}
  get salary() {return this._salary;}
  set name(value) {this._name = value;}
  set age(value) {this._age = value;}
  set salary(value) {this._salary = value;}
}
class Programmer extends Employee {
  constructor(name, age, salary, ...args) {
    super(name, age, salary);
    this.lang = Array.prototype.slice.call(args);
  }
    get salary() {
    return this._salary * 3;
  }
}
console.log(new Programmer('Ivan', 32, 2500, 'JS', 'Golang', 'Python', 'Java'));
console.log(new Programmer("Piter", 28, 2300, "Python", "Java", "C++"));
console.log(new Programmer("Andrew", 36, 2800, "Python", "Java", 'JS', "Golang", "C++"));